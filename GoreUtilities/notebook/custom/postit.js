// convert current notebook to html by calling "ipython nbconvert"
"using strict";

var do_post_it = (function() {
    doNbconvert = function(){
        var kernel = IPython.notebook.kernel;
        var name = IPython.notebook.notebook_name;
        IPython.notebook.save_checkpoint();
        command = 'import os; os.system(\"postit.py ' + name + '\")';
        kernel.execute(command);

    };

    IPython.toolbar.add_buttons_group([
        {
            id : 'doPostIt',
            label : 'Posts the current ipython notebook on wordpress',
            icon : 'icon-flag',
            callback : doNbconvert
        }
    ]);
})();
