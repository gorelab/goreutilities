import os, glob, httplib2, getpass, pickle, tempfile
from oauth2client.client import flow_from_clientsecrets
from oauth2client.keyring_storage import Storage
from apiclient.discovery import build
from apiclient.http import MediaFileUpload
from apiclient import errors
#import time
#import urllib
#import webbrowser

# Has user granted gdrive access?
# Assuming keyring storage available
#TODO Check if keyring storage is available, if not store credentials in a file (less preferable)
storage = Storage('wppost', getpass.getuser())
credentials = storage.get()
if credentials is None:
    print 'Need to authorize app on Google Drive'
    flow = flow_from_clientsecrets('client_secrets.json',
                                   scope='https://www.googleapis.com/auth/drive',
                                   redirect_uri='urn:ietf:wg:oauth:2.0:oob')
    auth_uri = flow.step1_get_authorize_url()
    print 'Authorize app by clicking here:'
    print auth_uri
    code = raw_input('Enter verification code: ').strip()
    credentials = flow.step2_exchange(code)
    storage.put(credentials)
print 'Google drive authentication successful'
gfid = {}
#storage.delete()
# To remove credentials file, run the following:
# from GoreUtilities.notebook import gdrive_api
# sys.modules['GoreUtilities.notebook.gdrive_api'].storage.delete()

def gdrive_create_folder(service, foldername, parent_id):
    """
    Creates a folder in gdrive if it does not already exist
    Returns: folder id
    """
    folders = service.children().list(folderId=parent_id, q="title = '"+foldername+"'").execute()['items']
    if len(folders) == 0:
        body = {"title": foldername, "parents": [{"id":parent_id}], "mimeType": "application/vnd.google-apps.folder"}
        file = service.files().insert(body=body).execute()
        return(file)
    else:
        return(folders[0])

def gdrive_update_file(service, directory, filename, parent_id, created=0):
    """
    Updates file on gdrive if local copy is newer
    As of now, simply deletes the file if it exists and uploads it again
    Returns: gdrive file object
    """
    files = service.children().list(folderId=parent_id, q="title = '"+filename+"'").execute()['items']
    if len(files) == 0:
        mimetype = mimedict.get(os.path.splitext(filename)[1],'default/default')
        media_body = MediaFileUpload(os.path.join(directory,filename), mimetype=mimetype, resumable=True)
        body = {"title": filename, "parents": [{"id":parent_id}], "mimeType":mimetype}
        filen = service.files().insert(body=body, media_body=media_body).execute()
        return(filen)
    else:
        # Not checking for modifications, all files are deleted and reuploaded
        # TODO: Upload only modified files
        #modDate = files[0]['modifiedDate']
        service.files().delete(fileId=files[0]['id']).execute()
        mimetype = mimedict.get(os.path.splitext(filename)[1],'default/default')
        media_body = MediaFileUpload(os.path.join(directory,filename), mimetype=mimetype, resumable=True)
        body = {"title": filename, "parents": [{"id":parent_id}], "mimeType":mimetype}
        filen = service.files().insert(body=body, media_body=media_body).execute()
        return(filen) 

# Dictionary to convert extensions to standard mime types
mimedict = {
            '.txt' :'text/plain',
            '.md'  :'text/plain',
            '.png' :'image/png',
            '.jpg' :'image/jpeg',
            '.jpeg':'image/jpeg',
            '.xls' :'application/vnd.ms-excel',
            '.xlsx':'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            '.xml' :'text/xml',
            '.ods' :'application/vnd.oasis.opendocument.spreadsheet',
            '.csv' :'text/plain',
            '.pdf' :'application/pdf',
            '.gif' :'image/gif',
            '.bmp' :'image/bmp',
            '.doc' :'application/msword',
            '.mp3' :'audio/mpeg',
            '.zip' :'application/zip',
            '.rar' :'application/rar',
            '.tar' :'application/tar',
            '.html':'text/html',
            '.htm' :'text/html',
            '.ipynb' : 'text/plain'
            }

def move_to_gdrive(path): # assuming path to .ipynb is being passed
    """
    Move contents of project folder (pointed to by path) to gdrive
    """
    path = os.path.dirname(os.path.abspath(path).strip('.md'))
    #print path
    PROJECT = os.path.basename(path)
    PROJECTPATH = os.path.join(os.path.dirname(path),PROJECT)
    print 'Project = ' + PROJECT + '\nPath = ' + PROJECTPATH
    sharePermission = {'value': '', 'type': 'anyone', 'role': 'reader'}
    http = httplib2.Http()
    http = credentials.authorize(http)
    drive_service = build('drive', 'v2', http=http)
    
    # Create wppost folder if it is absent
    wpf = gdrive_create_folder(drive_service, 'wppost', 'root')
    # Create project folder inside wppost if it is absent
    pf = gdrive_create_folder(drive_service, PROJECT, wpf['id'])
    # Update files
    gfid = {}
    gfid[PROJECTPATH] = pf['id']
    for dirt,folders,files in os.walk(PROJECTPATH):
        #print 'directory: ' + dirt#, folders, files
        # One way sync, nothing is deleted from gdrive
        # Create subdirectories and put their ids in a dictionary
        for folder in folders:
            #print 'folder: ' + folder
            gfid[os.path.join(dirt,folder)] = gdrive_create_folder(drive_service,folder,gfid[dirt])['id']
        # Search and modify files if required
        for filename in files:
            #print 'file: ' + filename
            #print time.ctime(os.path.getmtime(os.path.join(dirt,filename)))
            # Cannot upload empty files
            if os.stat(os.path.join(dirt,filename)).st_size > 0:
                filen = gdrive_update_file(drive_service,dirt,filename,gfid[dirt])
                # Set permissions
                drive_service.permissions().insert(fileId=filen['id'], body=sharePermission).execute()
    gfid[os.path.dirname(PROJECTPATH)] = wpf['id']
    filen = gdrive_update_file(drive_service,os.path.dirname(PROJECTPATH),PROJECT+'.ipynb',gfid[os.path.dirname(PROJECTPATH)])
    drive_service.permissions().insert(fileId=filen['id'], body=sharePermission).execute()
    return gfid

def get_url(path, **kwargs):
    gfid = kwargs['keys']
    path = os.path.abspath(path)
    #print gfid.keys()
    #print os.path.dirname(path)
    #print path
    http = httplib2.Http()
    http = credentials.authorize(http)
    drive_service = build('drive', 'v2', http=http)
    files = drive_service.children().list(folderId=gfid[os.path.dirname(path)], q="title = '"+os.path.basename(path)+"'").execute()['items']
    return('https://googledrive.com/host/'+files[0]['id'])
    #return(drive_service.files().get(fileId=files[0]['id']).execute()['webContentLink'])