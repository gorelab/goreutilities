"""
Includes a few utility functions to extract information from ipython notebooks.
"""
from IPython.nbformat import current as nbformat
from GoreUtilities import util
try:
	from IPython.nbconvert.transformers import ExtractOutputTransformer
	cell_processor = ExtractOutputTransformer().transform_cell
	processor_resources = lambda x: {}
except ImportError:
	from IPython.nbconvert.preprocessors import ExtractOutputPreprocessor
	cell_processor = ExtractOutputPreprocessor().preprocess_cell
	processor_resources = lambda x: {'outputs':None}	
from markdown_tools import get_meta_from_md, update_meta_in_md_txt
import os

def get_notebook_node_content(notebook_path):
    """
    Returns the notebook_node formatted notebook.

    Parameters
    -----------
    path to the ipython notebook
    """
    with open(notebook_path, 'r') as f:
        notebook = nbformat.read(f, 'ipynb')
    return notebook

def write_notebook(notebook_node_content, notebook_path):
    """ Writes the ipython notebook file with the tags. """
    with open(notebook_path, 'w') as f:
        nbformat.write(notebook_node_content, f,  'ipynb')


def process_cell_source(cell):
	'''
	Convert contents of cell to markdown depending on the cell type
	'''
	raw_source = cell['source']
	if cell[u'cell_type'] == u'code':
		source = raw_source
	elif cell[u'cell_type'] == u'heading':
		level = cell[u'level']
		source = '#'*level + ' ' + raw_source
	else:
		source = raw_source
	return source

def post_results_section(cells, output_dir):
    """
    Given notebook_node cells from an ipython notebook, extrats all the images before which there exists a markdown cell.
    The text of the markdown cell is placed before the images in the final markdown file.
    The images themselves are saved into
    the specified output_dir.

    Parameters
    -----------
    cells - notebook_node formatted cells
    output_dir - folder into which to output the images

    Returns
    -------------
    List of dictionaries. Each dictionary includes a description entry and a list of image paths.
    """    
    md_post = []
    for cell_index, cell in enumerate(cells):
		b, resources = cell_processor(cell, processor_resources(''), cell_index)
		keys = resources['outputs'].keys()
		keys.sort()
        #images = [k for k in keys if 'png' in k]
		images = filter(lambda x : 'png' in x, keys)
		if len(images) > 0:
			previous_cell = cells[cell_index-1]
			if previous_cell[u'cell_type'] in [u'markdown', u'heading']:
				source = process_cell_source(previous_cell)
				img_paths = map(lambda x : os.path.join(output_dir, x), images)
				md_post.append({'source' : source, 'image paths' : img_paths, 'image names' : images})
				for img in images:
				    with open(os.path.join(output_dir, img), 'wb') as f:
				        f.write(resources['outputs'][img])
		if cell[u'cell_type'] in [u'markdown', u'heading']:	
			source = process_cell_source(cell)		
			if cell_index == len(cells)-1:
			    md_post.append({'source' : source, 'image paths' : [], 'image names' : []})
			else:
				b, resources = cell_processor(cells[cell_index+1], processor_resources(''), cell_index+1)
				keys = resources['outputs'].keys()
				images = filter(lambda x : 'png' in x, keys)
				if len(images) == 0:
					md_post.append({'source' : source, 'image paths' : [], 'image names' : []})
		# search for html output
		if cell[u'cell_type'] == u'code':
		    for op in cell[u'outputs']:
		        if u'html' in op.keys():
		            md_post.append({'source' : op[u'html'], 'image paths' : [], 'image names' : []})
                    #print op[u'html']
        #print cell
    #print md_post
    return md_post

def produce_md_from_notebook(notebook_path, **tags):
    """
    Prouces an md post from the ipython notebook path.

    Parameters
    ------------
    notebook_path - path to ipython notebook

    Returns
    ------------

    path to md file and contents of file
    """
    # Create output folder
    basename = os.path.basename(notebook_path)
    barename = os.path.splitext(basename)[0]

    output_dir = barename
    util.ensure_directory(output_dir)

    tags.setdefault('open_tags', '')
    tags.setdefault('close_tags', '')

    # Load notebook
    notebook = get_notebook_node_content(notebook_path)

    # Ensure first cell is markdown
    if notebook.worksheets[0].cells[0]['cell_type'] != 'markdown':
        raise Exception('First cell in file {} must be a markdown cell'.format(notebook_path))

    # Extract images and image captions
    analysis_list = post_results_section(notebook.worksheets[0].cells, output_dir)

    # Put everything together into one string to be used as initial md post
    tmp = ''
    for section in analysis_list:
        tmp += u'\n\n{}\n\n'.format(section['source'])
        for img_name, img_path in zip(section['image names'], section['image paths']):
			tmp += '\n![{name}]({open_tags}{path}{close_tags})'.format(name=img_name, path=img_path, **tags)
	md_contents = tmp.strip()

    # Add link to ipython notebook
    md_contents += '\n\n#Full ipython notebook\n\n'
    db_notebook_path = os.path.abspath(notebook_path)
    md_contents += "Click [here]([nbview][db]{path}[/db][/nbview]) for ipython notebook\n\n".format(path=db_notebook_path)

    # Output results to file
    md_path = os.path.join(output_dir, barename) + '.md'
    with open(md_path, 'w') as f:
        f.write(md_contents.encode("UTF-8"))
    return md_path, md_contents


def update_notebook_meta_from_md(notebook_path, md_path):
    notebook = get_notebook_node_content(notebook_path)
    new_meta = get_meta_from_md(md_path)

    # Update meta in first cell
    if notebook.worksheets[0].cells[0]['cell_type'] != 'markdown':
        raise Exception('First cell in file {} must be a markdown cell'.format(notebook_path))
    first_cell_md = notebook.worksheets[0].cells[0]['source'] # Returns the source of the first cell

#     print first_cell_md
    new_md = update_meta_in_md_txt(first_cell_md, new_meta)
    notebook.worksheets[0].cells[0]['source'] = new_md
    write_notebook(notebook, notebook_path)

if __name__ == '__main__':
# 	path = '/host/home/yonatanf/Dropbox/PAIRWISE INTERACTIONS/src/2014-06-13.4_carbon_sources_dynamics.ipynb'
	path = '/host/home/yonatanf/Dropbox/PAIRWISE INTERACTIONS/src/2014-09-13.EXP_8-Infinite_test.ipynb'
	print produce_md_from_notebook(path)
